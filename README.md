# A program for tiling images automatically from the OpenFlexure Microscope

A simple Python program for tiling and stitching 2D scans into panoramic images. This program has relatively few dependencies, runs quickly with extremely low memory requirements. Currently relies on metadata from the stage stored according to OpenFlexure Metadata standards in an ```exif``` format.

The main application of this program is for automatic pipelines requiring minimal user input. Although there are a range of settings in the code, not all of them are currently exposed to the user. Possibly want different branches for experts and non-experts.

This work will form the basis of a paper on intuitive and efficient image tiling.

A suitable example dataset is shared at https://doi.org/10.5281/zenodo.13768403. 

## Obtaining the script

Until this is published to PyPI, you may install the script with:

```
pip install git+https://gitlab.com/openflexure/openflexure-stitching.git
```

It requires at least Python 3.9, other dependencies should be declared in `pyproject.toml` (and automatically installed by pip).  It is our intention to distribute the module on PyPI, and also in binary form for Windows users, with VIPS bundled to avoid PATH-related issues.

## Development

Clone this repository by navigating in your terminal to your required containing folder. Run 
```
git clone https://gitlab.com/openflexure/openflexure-stitching
```
to download the files locally. 

Create and activate a virtual environment:
```
cd openflexure-stitching
python -m venv .venv
.\.venv\Scripts\activate
```
Upgrade `pip` if needed, so you can use the `pyproject.toml` based setup for an editable install
```
python -m pip install --upgrade pip
```
Install the necessary libraries using 
```
pip install -e .
```
**At this point, you should be ready for tiling _except_ exporting as an OME Pyramidal TIFF file.**

### VIPS

To generate pyramidal TIFF files we use the [`pyvips` library](https://github.com/libvips/pyvips). For `pyvips` to work, you'll also need to [install `libvips`](https://www.libvips.org/install.html), and ensure that it is available on your PATH.

On non-Linux systems `libvips` should be installable via the package manager for your system. For example using the command

    sudo apt install libvips42

On MacOS [homebrew](https://brew.sh/) is required. With homebrew installed run:

    brew install vips python pkg-config

On Windows, download [libvips](https://github.com/libvips/libvips/releases). Unzip the `vips-dev-8.x` folder to the root directory of this repository. If you require `vips` to be available anywhere on your sustme you can [add this directory to your PATH](https://www.computerhope.com/issues/ch000549.htm).

## Requirements

OpenFlexure Stitching relies on an estimate of the location of all images in the scan. This estimate doesn't need to be too accurate (see [Loading in images](#loading-in-images)), but does need to give an idea of which images overlap.

The program currently looks for this data in 

```
piexif.load(<filepath>)["Exif"][piexif.ExifIFD.UserComment]['instrument']['state']['stage']['position']
```

This is the location according to the stage of the system, which might not line up with the pixel locations (two images might be several thousand motor steps apart, but only a few hundred pixels). To correct between the two, we use a "Camera-to-Stage" mapping matrix. This is a 2x2 matrix which maps from stage coordinates to rough image coordinates, located at 

```
piexif.load(<filepath>)["Exif"][piexif.ExifIFD.UserComment]['instrument']['settings']['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement']
```

Currently without these, the program can't run. Two features to add (and I welcome pull requests for this), are getting image coordinates from elsewhere (maybe the filename, or an external file?) and not needing a CSM estimate. Instead, choosing a suitable value such that all images are assumed to overlap with their nearest neighbours only.

## Running the program

This program is designed to work without user input - ideally there'll be no need for users to insert thresholds before running. If you have a collection of images with the metadata outlined in [requirements](#requirements), then from the terminal, running
```
openflexure-stitch <link to folder of images>
```
will hopefully tile the images together. Consulting `openflexure-stitch --help` will list available settings and options.

We will add a command-line option to manually set cutoff thresholds, and to generate a plot showing how many correlations would be excluded. This means thresholds are set once the values they apply to are known - there's no need for the user to guess them in advance.

## How it works

This is an overview of how the program currently works. Major code rewrites are needed to organise functions more sensibly, allow non-OpenFlexure metadata, and expose the settings for users to modify if necessary. Long term, we'll also want a GUI to run the program, modify settings and preview ongoing scans.

### Loading in images

OpenFlexure Stitching relies on the ```opencv``` library to open the input images. This makes it compatible with most common image types. Given a folder path, it will import a list of all images in the folder, ignoring any containing phrases from stitching outputs (```'stitched', 'comparison', 'Fused', 'stage'``` are all ignored)

From these images, the locations and CSM matrix are extracted. At this point, we can compare the inputs settings, image list and timestamps to a cache, located at ```./export.pkl```. This is one of the key advantages of OpenFlexure Stitching; if the settings and images are unchanged, we don't need to repeat the slow, intensive offset-identification steps. Even if new images have been added to the folder, we'll still reuse as much as possible from the cache. This means live-updating scans can be tiled in near realtime.

Assuming the cache is absent or settings have changed, we need to process the images. Based on their location, the CSM and the ```fractional_overlap``` parameter, overlapping images are identified.

### Offset estimation

The offset between each pair of overlapping images can be found from the product of the Fourier transforms of the two images. We find this for every image, along with a confidence metric. For this step, there are two parameters that are key to making OpenFlexure Stitching efficient and adaptable.

- ```pad``` - when finding the correlation between images, the images can either be padded or not. Padding involves adding an empty frame to surround an image before finding the correlation. This means that every possible offset location is tested, with no ambiguity in the result. The alternative is to not pad the image, keeping it the original size. In this case, one image will be wrapped when the correlation is performed, leading to an ambiguous peak corresponding to four possible offsets. This ambiguity can be broken by then manually testing each option, but this isn't 100% reliable...  
**TLDR:** padding appears more reliable, but uses ~4x the memory. Not padding is more efficient, and is the default for images with a lot of overlap
- ```priority``` - options are ```time``` or ```memory```. OpenFlexure Stitching is designed to be low-demand on the system, and the ```memory``` setting limits the data held in memory to just the two images currently being aligned. ```time``` will instead allow the system to hold multiple Fourier-transform images in memory until all relevant overlaps have been found  
**TLDR:** ```time``` is roughly twice the speed of ```memory```, but for a scan with 14MB JPEGs, ```memory``` RAM requirements max out at 650MB instead of 1,800MB

For each pair, the location of the estimated offset, the confidence in the correlation and the discrepancy between the stage-recorded offset and the correlation offset is found and cached.

<figure>
<img src="README_images/2d.png" style="max-width:40%;" />
<img src="README_images/2d_fail.png" style="max-width:40%;" />
<figcaption>Left: a correlation map with a single, narrow peak. Useful for tiling. <br> Right: a correlation map with no clear peak or preference for offset. Should be filtered out. </figcaption>
</figure>

### Thresholding

The correlation peaks between some pairs of images will inevitably be incorrect, and need to be thresholded out. Whether the thresholding is manual or automatic - unlike in other programs - it's set once all values are known.

<figure>
<img src="README_images/scatter.png" style="max-width:79%;" />
<figcaption>A scatter plot of the quality of each correlation between two images. Users have the option to set x and y thresholds to remove bad overlaps. Colours are for clarity and have no data significance. </figcaption>
</figure>

A manual threshold can be set by the user, while an automatic threshold will be chosen automatically based on the best constrained positions. 

From a list of accepted (within the thresholds) offsets between images, the least squares fit is found. This positions all images based on the average of their accepted offsets. The RMS error of the resulting locations is found, and is used as a measure of how successful the fit is. When setting a threshold automatically, every possible threshold is tested, and the one with the lowest RMS error while still locating every tile in the scan is used.

At this point, the locations are all images are set, and the program is ready to begin stitching.

### Stitching images

There's a few different options for image stitching. The simplest is to simply export a ```PyTileConfig.txt``` file, which can be read by Fiji Stitching, taking advantage of their range of blending and averaging. To avoid producing artefacts in medical scans, the stitching in OpenFlexure Stitching does no averaging between images.

The images can also either be placed directly, leading to clear boundaries between images, or cropped so that only the centre of images are used. This smooths out some vignetting, and means we use the high quality centre of each field of view.

Images can be exported as either a JPEG or pyramidal TIFF. Other export formats from ```opencv``` would be pretty straightforward to add if there was any benefit to offering them. A JPEG (or other opencv formats) are straightforward to use, but lack the efficiency of Pyramidal TIFFs. When opened in microscopy programs such as Qupath, important metadata such as the physical pixel size will be used for labelling the image.

Keeping the memory requirements of stitching a giga-pixel image low is challenging. For the Pyramidal TIFF export program, it splits the tiled image into smaller chunks, which Pyvips efficiently stitches. The memory vs speed of this step requires some investigation.

# Developer notes

## Building an executable

On Windows, you can build a single-file .exe using `pyinstaller`:

1. Download VIPS, and extract the `vips-dev-xx` folder into the project's root. NB the `bin` directory should be located at `vips-*/bin/` (don't have multiple nested `vips-*` folders).
2. Activate the virtual environment for this project
3. Run `python build_exe.py`

I have only tested this on Windows. Other platforms are not tested yet.