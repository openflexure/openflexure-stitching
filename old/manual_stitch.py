# %%
import logging
import os
import cv2
import numpy as np
import json
import piexif
import piexif.helper
import reconstruct_tiled_image
import matplotlib.pyplot as plt
import sys
from memory_profiler import memory_usage
import pickle
import time
import stitching_methods


def find_overlaps(folder_path):
    img_list = reconstruct_tiled_image.get_img_list(folder_path=folder_path)
    data = reconstruct_tiled_image.pull_usercomment_dict(os.path.join(folder_path, img_list[0]))
    camera_to_sample = np.array(data['instrument']['settings']['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement'])
    print(img_list)
    print(camera_to_sample)
    downsample = 1
    resize = 1
    unknown_images = "warn"
    fractional_overlap= 0.2
    img_limit = 0
    pad = False

    pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre = reconstruct_tiled_image.reconstruct_tiled_image(
        folder_path,
        img_list,
        camera_to_sample,
        resize, 
        unknown_images, 
        fractional_overlap,
        img_limit,
        pad,
        use_cache=True,
        priority='time')

    reconstruct_tiled_image.plot_overlaps(pairs, pair_data, pixel_positions, block=True)
    x_thresh = float(input('Insert x threshold: '))
    y_thresh = float(input('Insert y threshold: '))


    tiles, positions, csm, scan_centre = reconstruct_tiled_image.stitch_from_thresholds(
        folder_path,
        pairs,
        pair_data,
        pixel_positions,
        tiles,
        unknown_images,
        camera_to_sample,
        resize,
        fractional_overlap,
        starting_pixel_positions,
        scan_centre,
        x_thresh,
        y_thresh,
        img_limit,
        downsample)
    
    reconstruction = reconstruct_tiled_image.generate_stitched_image(tiles, positions, resize, downsample, csm, scan_centre)
    cv2.imwrite(os.path.join(folder_path, f"stitched_{img_limit}.jpg"), cv2.cvtColor(reconstruction['stitched_image'][:,:,::-1], cv2.COLOR_BGR2RGB))

    image, _, _ = stitching_methods.stitch_centre_of_images(tiles, positions/resize, downsample=downsample)
    cv2.imwrite(os.path.join(folder_path, f"centres_stitched_{img_limit}.jpg"), cv2.cvtColor(image[:,:,::-1], cv2.COLOR_BGR2RGB))

    with open(os.path.join(folder_path, 'PyTileConfig.txt'), 'w') as fp:
        fp.write('# Define the number of dimensions we are working on\ndim = 2\n\n# Define the image coordinates\n')
        for i in range(len(positions)):    
            loc = positions[i] / resize + [scan_centre[0], scan_centre[1]]
            fp.write(f'{img_list[i]}; ; {loc[1], loc[0]} \n')

    return 0

if __name__ == '__main__':
    timings = {}
    for folderpath in sys.argv[1:]:
        start = time.time()
        mem_usage = memory_usage((find_overlaps, (folderpath, ),  ))
        # find_overlaps(folderpath)
        
        timings[f'repeat {folderpath}'] = time.time() - start
        print(timings)
        
        # tile_compare.compare(folderpath)
    
        print('Maximum memory usage: %s' % max(mem_usage))

        plt.plot(mem_usage)
        plt.show(block=True)

