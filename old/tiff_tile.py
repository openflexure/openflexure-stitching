# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 15:53:08 2021

@author: Ajay Zalavadia (zalavadia.ajay@gmail.com)_
"""
import os
import time
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import tifffile
import json
import numpy as np
import pickle

vipshome = r'C:\Users\Administrator\Downloads\vips-dev-w64-all-8.14.2\vips-dev-8.14\bin'
os.environ['PATH'] = vipshome + ';' + os.environ['PATH']
import pyvips
print("vips version: " + str(pyvips.version(0))+"."+str(pyvips.version(1))+"."+str(pyvips.version(2)))

# imagedir= 'D:\\Merge-Test'
# outFile = os.path.join(imagedir, 'stitched.ome.tif')
# fileList = glob.glob(imagedir+'/*component_data.tif')

#Define useful stuff
def print_vipsinfo(filePath):
    str1 = filePath.split("_[", 2)
    str2 = str1[1].split("]_",2)
    xy = str2[0].split(",",2) # get image location from file name
    x = pyvips.Image.new_from_file(filePath, access='sequential')
    #print(x.get("image-description"))
    print('interpretation: ',x.interpretation)
    print('height: ',x.height)
    print('width: ',x.width)
    print('image format: ',x.format)
    print('bands: ',x.bands)
    print('pages: ',x.get("n-pages"))
    print('vips-loader: ',x.get("vips-loader"))
    print('X resolution: ',x.get("xres"))
    print('Y resolution: ',x.get("yres"))
    print('Resolution unit: ',x.get('resolution-unit'))
    print('X position: ',x.get('xoffset'))
    print('Y position: ',x.get('yoffset'))
    print('X: ',xy[0],' Y: ',xy[1])
    
def print_tiffinfo(filePath):
    str1 = filePath.split("_[", 2)
    str2 = str1[1].split("]_",2)
    xy = str2[0].split(",",2) # get image location from file name
    with tifffile.TiffFile(filePath) as tif:
        interpretation = tif.pages[0].tags['PhotometricInterpretation']
        height = tif.pages[0].tags['ImageLength']
        width = tif.pages[0].tags['ImageWidth']
        imgformat = tif.pages[0].tags['BitsPerSample']
        pages = len(tif.pages)
        xres = tif.pages[0].tags['XResolution']
        yres = tif.pages[0].tags['YResolution']
        resunit = tif.pages[0].tags['ResolutionUnit']
        xpos = tif.pages[0].tags['XPosition']
        ypos = tif.pages[0].tags['YPosition']
    
    print('interpretation: ',interpretation.value)
    print('height: ',height.value)
    print('width: ',width.value)
    print('image format: ',imgformat.value)
    print('pages: ',pages)
    print('X resolution: ',xres.value)
    print('Y resolution: ',yres.value)
    print('Resolution unit: ',resunit.value)
    print('Position file name X: ',xy[0],' Y: ',xy[1])
    print('Position tiff tag X: ',10000*xpos.value[0]/xpos.value[1],' Y: ',10000*ypos.value[0]/ypos.value[1])
    print('micron per pixel X:',xres.value[0]/(xres.value[1]*10000))
    print('micron per pixel Y:',yres.value[0]/(yres.value[1]*10000))

class region:
    def __init__(self, XPosition, YPosition, Height, Width, filePath, XResolution, YResolution,XCord,YCord):
        self.XPosition=XPosition # Stage position 
        self.YPosition=YPosition # Stage position
        self.Height=Height
        self.Width=Width
        self.filePath=filePath
        self.XResolution=0
        self.YResolution=0
        self.ResolutionUnit = 'cm'
        self.XCord = XCord # pixel position on the stitched image
        self.YCord = YCord # pixel position on the stitched image

def interleaved_tile(filePath,channels):
    tile = pyvips.Image.new_from_file(filePath)
    page_height = tile.height
    # chop into pages
    pages = [tile.crop(0, y, tile.width, page_height) for y in range(0, tile.height, page_height)]
    # join pages band-wise to make an interleaved image
    tile = pages[0].bandjoin(pages[1:])
    tile = tile.copy(interpretation="multiband")
    return tile

def save_ometiff(stitched_img,outFile,pixel_size):
    # collect image dimension needed for OME-XML before separating image planes
    width = stitched_img.width
    height = stitched_img.height
    bands = stitched_img.bands

    print('Final Image dimentions(WxHxC):',width,height,bands)
    # split to separate image planes and stack vertically for OME-TIFF 
    stitched_img = pyvips.Image.arrayjoin(stitched_img.bandsplit(), across=1)
    res = 10**3 / pixel_size
    pixel_size /= 10**3
    #Set tiff tags necessary for OME-TIFF
    stitched_img = stitched_img.copy(xres=res, yres=res)

    # build minimal OME metadata. TODO: get calibration and channel names
    stitched_img.set_type(pyvips.GValue.gstr_type, "image-description",
    f"""<?xml version="1.0" encoding="UTF-8"?>
    <OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2016-06"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2016-06 http://www.openmicroscopy.org/Schemas/OME/2016-06/ome.xsd">
        <Image ID="Image:0">
            <!-- Minimum required fields about image dimensions -->
            <Pixels DimensionOrder="XYCZT"
                    ID="Pixels:0"
                    PhysicalSizeX= "{pixel_size}"
                    PhysicalSizeY= "{pixel_size}"
                    PhysicalSizeXUnit= "mm"
                    PhysicalSizeYUnit= "mm"
                    SizeC="{bands}"
                    SizeT="1"
                    SizeX="{width}"
                    SizeY="{height}"
                    SizeZ="1"
                    Type="float">
            </Pixels>
        </Image>
    </OME>""")

    stitched_img.set_type(pyvips.GValue.gint_type, "page-height", height)
    stitched_img.write_to_file(outFile, compression="lzw", tile=True, tile_width=512, tile_height=512,  pyramid=True, subifd=True, bigtiff=True)
    print(f'xres is {stitched_img.xres}')
    print(f'yres is {stitched_img.yres}')
    return 0

#Stitch regions and return ome-tiff compatible image
def stitch_regions(img_list, positions, folder_path, outFile):
    try:
        with open(os.path.join(folder_path, 'metadata.pkl'), 'rb') as f:
            data = pickle.load(f)
    
        pixel_size = data['instrument']['settings']['extensions']['usafcal']['um_per_px']
    except:
        pixel_size = 1
    print(f'{pixel_size} \n')
    stitched_img = pyvips.Image.black(1, 1,bands=3) 
    positions = np.array(positions)
    print(positions)
    positions -= np.min(positions, axis = 0)
    for i, img in enumerate(img_list):
        img = os.path.join(folder_path, 'split', img)
        tile = interleaved_tile(img,stitched_img.bands)
        # insert tile to build final image
        stitched_img = stitched_img.insert(tile, positions[i][1],positions[i][0],expand=1,background=[0])
        
    stitched_img = save_ometiff(stitched_img,outFile, pixel_size)

    return 0

def load_python_positions(locations_path):
    with open(locations_path, 'rb') as fp:
        python = json.load(fp)

    all_x = []
    all_y = []
    locs = []
    
    for loc in python.values():
        all_x.append(loc[0])
        all_y.append(loc[1])
        locs.append([loc[0], loc[1]])

    images = list(python.keys())
    return images, locs

import reconstruct_tiled_image

def produce_tiff(folder_path):
    subfolder_path = os.path.join(folder_path, 'split')
    locations_path = os.path.join(subfolder_path, "locations.json")
    outFile = os.path.join(subfolder_path, 'stitched.ome.tif')

    #Do stuff
    t0 = time.time()
    img_list, positions = load_python_positions(locations_path)
    t1 = time.time()

    stitch_regions(img_list, positions, folder_path, outFile)
    t2= time.time()

    print('Time to collect regions: ',t1-t0)
    print('Time to stitch and save: ',t2-t1)
