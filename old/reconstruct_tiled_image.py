# -*- coding: utf-8 -*-
"""
Reconstruction of tiled images

@author: rwb27 and jaknapper
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import cv2
import os
import json
import piexif
import logging
from datetime import datetime
import pickle 

from sklearn import linear_model
from camera_stage_mapping import initial_fft, displacement_from_fft_template, high_pass_fourier_mask
from typing import List

from .metadata import ImageMetadata, read_metadata_from_file

from alive_progress import alive_bar
import time
from memory_profiler import profile

import sys

def find_images(folder_path: str) -> List[str]:
    """List all images in a folder, excluding ones that are output."""

    # Get a list of all images in the folder, excluding ones containing any of 'ignore_phrases'
    ignore_phrases = ['stitched', 'comparison', 'Fused', 'stage']

    return [
        f for f in os.listdir(folder_path) 
        if os.path.isfile(os.path.join(folder_path,f)) 
        and f.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')) 
        and not any(phrase in f for phrase in ignore_phrases)
    ]

def load_metadata(folder_path: str, images: List[str], filter_sample: bool=True) -> List[ImageMetadata]:
    """Load metadata for a list of images, in a standardised form
    
    Currently this expects OpenFlexure style metadata, i.e. a JSON blob in the
    usercomment exif field. Failing that, it falls back to position in the filename
    and time from the `ctime`.
    """
    metadata_list: List[ImageMetadata] = [
        read_metadata_from_file(os.path.join(folder_path, image_fname))
        for image_fname in images
    ]
    metadata_list.sort(key=lambda x: x.timestamp, reverse=False)

    # Some scans will mark images as either sample or background. If some are marked as sample, only tile those
    if filter_sample:
        sample_list = [m for m in metadata_list if "sample" in m.tags]
        if len(sample_list) > 0:
            metadata_list = sample_list

    #TODO: this function previously grouped images by XY position and filtered only the sharpest.
    # RWB removed this function as it wasn't being used.

    return metadata_list


def pixel_positions_from_metadata(image_metadata: List[ImageMetadata], resize):
    """Estimate pixel positions, based on the stage_position of each image"""
    stage_positions = [m.stage_position[:2] for m in image_metadata]
    centre = np.mean(stage_positions, axis=0)[:2]
    rel_positions = np.array(stage_positions) - centre
    pixel_positions = resize * np.dot(rel_positions[:,:2], 
                             np.linalg.inv(px_per_step)))

def get_pixel_positions(dir, img_list, px_per_step, resize, img_limit = 0):
    """Given a list of images, extract the relative positions of the images.
    """
    for link in img_list:
        link = os.path.join(dir, link)
        try:
            data = pull_usercomment_dict(link)

            x_stage, y_stage, z_stage = (data['instrument']['state']['stage']['position'].values())

            timestamps.append(data['image']['time'])
        except:
            try:
                loc = pull_loc(link)
                x_stage, y_stage = loc.split(',')
            except:
                raise Exception

        images.append(link)
        positions.append([x_stage, y_stage])
    
    img = cv2.imread(images[0], 0)

    centre = np.mean(positions, axis=0)[:2]

    positions = np.array(positions) - centre[:2]

    pixel_positions = resize * np.dot(positions[:,:2], 
                             np.linalg.inv(px_per_step)) * img.shape[:2] / np.asarray([624, 832])
    
    return images, pixel_positions, centre, positions, timestamps


def compareplot(a,b, gain=10, axes=None):
    """Plot two sets of coordinates, highlighting their differences.
    
    a, b: numpy.ndarray
        An Nx2 array of points
    gain: float
        The amount to amplify differences when plotting arrows
    axes: matplotlib.Axes
        An axes object in which we make the plot - otherwise a new one
        will be created.
    """

    centre = np.mean(a, axis=0)[:2]
    a = np.array(a) - centre[:2]

    a = a * gain

    if axes is None:
        f, axes = plt.subplots(1,1)
    axes.plot(a[:,0],a[:,1], '.')
    axes.plot(b[:,0],b[:,1], '.')
    for ai, bi in zip(a,b):
        axes.arrow(ai[0],ai[1],(bi[0]-ai[0])*gain,(bi[1]-ai[1])*gain)


def find_overlapping_pairs(pixel_positions, image_size,
                           fractional_overlap=0.1):
    """Identify pairs of images with significant overlap.
    
    Given the positions (in pixels) of a collection of images (of given size),
    calculate the fractional overlap (i.e. the overlap area divided by the area
    of one image) and return a list of images that have significant overlap.
    
    Arguments:
    pixel_positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    image_size: numpy.ndarray
        An array of length 2 giving the size of each image in pixels.
    fractional_overlap: float
        The fractional overlap (overlap area divided by image area) that two
        images must have in order to be considered overlapping.
    
    Returns: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
        The second int will be larger than the first.
    """
    tile_pairs = []
    for i in range(pixel_positions.shape[0]):
        for j in range(i+1, pixel_positions.shape[0]):
            overlap = image_size - np.abs(pixel_positions[i,:2] -
                                          pixel_positions[j,:2])
            overlap[overlap<0] = 0
            tile_pairs.append((i, j, np.product(overlap)))
    overlap_threshold = np.prod(image_size)*fractional_overlap
    overlapping_pairs = [(i,j) for i,j,o in tile_pairs if o>overlap_threshold]

    for pair in overlapping_pairs:
        if pair[0] > pair[1]:
            print(pair)

    return overlapping_pairs


#################### Cross-correlate overlapping pairs to match them up #######
def crosscorrelate_overlapping_images(tiles, overlapping_pairs, 
                                     pixel_positions, 
                                     pad = False,
                                     resize=0.6,
                                     cache = {},
                                     high_pass_sigma=50,
                                     priority='time'):
    """Calculate actual displacements between pairs of overlapping images.
    
    For each pair of overlapping images, perform a cross-correlation to
    fine-tune the displacement between them.
    
    Arguments:
    tiles: [h5py.Dataset]
        A list of datasets (or numpy images) that represent the images.
    overlapping_pairs: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
    pixel_positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    
    Results: np.ndarray
        An Mx2 array, giving the displacement in pixels between each pair of
        images specified in overlapping_pairs.
    """
    pair_data = {}

    initial_ffts = {}

    def load_image(i):
        """Load and resize an image"""
        return cv2.resize(cv2.imread(tiles[i], -1) , dsize = [0,0], fx = resize, fy = resize)

    img_0 = load_image(0)
    image_size = img_0.shape[:2]
    initial_fft_0 = initial_fft(img_0, pad=pad)
    high_pass_filter = high_pass_fourier_mask(initial_fft_0.shape, high_pass_sigma)

    del img_0, initial_fft_0

    with alive_bar(len(overlapping_pairs), force_tty=True, dual_line=False) as bar:  # declare your expected total
        for h, (i, j) in enumerate(overlapping_pairs):
            bar.text = f'-> Finding overlap between: {i} and {j}'
            if not cache or f'{i}, {j}' not in cache['pairs'].keys():

                original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])
                overlap_size = image_size - np.abs(original_displacement)
                assert np.all(overlap_size <= image_size), "Overlaps can't be bigger than the image"
                if priority == 'time':
                    if not i in initial_ffts:
                        initial_ffts[i] = initial_fft(load_image(i), pad=pad)
                    
                    if not j in initial_ffts:
                        initial_ffts[j] = initial_fft(load_image(j), pad=pad)

                    trial_peak, corr, max_value, max_index = displacement_from_fft_template(
                    np.conj(initial_ffts[i]) * high_pass_filter,
                    initial_ffts[j],
                    pad=pad,
                    fractional_threshold=0.02,
                    return_peak=True
                    )

                else:
                    trial_peak, corr, max_value, max_index = displacement_from_fft_template(
                        np.conj(initial_fft(load_image(i), pad = pad)) * high_pass_filter,
                        initial_fft(load_image(j), pad = pad),
                        pad=pad,
                        fractional_threshold=0.02,
                        return_peak=True
                    )
                
                # plot_corrs(corr)
                
                pair_data[f'{i}, {j}'] = {}

                if not pad:
                    trial_peak = break_ties(
                        cv2.imread(tiles[i], 0), 
                        cv2.imread(tiles[j], 0), 
                        np.unravel_index(max_index, np.array(corr).shape), 
                        plot=False,
                    )

                trial_peak *= -1

                pair_data[f'{i}, {j}']['peak loc'] = trial_peak
                pair_data[f'{i}, {j}']['transform'] = np.sum(np.abs(np.array((np.array(trial_peak) - original_displacement))))
                

                # for thresh in [0.5, 0.75, 0.9, 0.95]:
                for thresh in [0.9]:
                    # This is doing:
                    # x = (np.min(corr) + thresh * (np.max(corr) - np.min(corr)))
                    # background_subtracted = corr - x
                    # background_subtracted = corr - (np.min(corr) + thresh * (np.max(corr) - np.min(corr)))
                    # under_thresh = np.count_nonzero(background_subtracted < 0)
                    # pair_data[f'{i}, {j}'][f'{thresh} thresh'] = under_thresh / np.count_nonzero(corr)

                    corr_size = np.count_nonzero(corr)
                    corr = corr - (np.min(corr) + thresh * (np.max(corr) - np.min(corr)))
                    under_thresh = np.count_nonzero(corr < 0)

                    pair_data[f'{i}, {j}'][f'{thresh} thresh'] = under_thresh / corr_size

                adjusted_peak = trial_peak.copy()
        
                original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])
                
                transform = np.array((np.array(trial_peak) - original_displacement))
                transform = np.sum(np.abs(transform))

                del corr
                del under_thresh
                if priority == 'time':
                    if not any(i in pair for pair in overlapping_pairs[h+1:]):
                        del initial_ffts[i]

                    if not any(j in pair for pair in overlapping_pairs[h+1:]):
                        del initial_ffts[j]
                    
            else:
                pair_data[f'{i}, {j}'] = cache['pairs'][f'{i}, {j}']
                
            bar()
        # if not any(i in sublist for sublist in overlapping_pairs[h+1:]):
        #     del templates[i]

        # plot_pairs(trial_peak, img_i, img_j, i, j, corr, 'white', 140)

    return pair_data

def plot_corrs(corr):
    '''Plots a 2d and 3d representation of the correlation map
    Used for figures in paper and README'''
    corr = cv2.resize(corr, dsize=[0,0], fx=0.1, fy=0.1)
    corr = corr - np.min(corr)
    corr = corr / np.max(corr)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    to_plot_x, to_plot_y = np.mgrid[0:corr.shape[0], 0:corr.shape[1]]

    ax.plot_surface(to_plot_x, to_plot_y, corr, rstride=1, cstride=1, color = 'c', linewidth=0)
    ax.view_init(elev=15)
    xx, yy = np.meshgrid(np.linspace(0,corr.shape[0], 2), np.linspace(0,corr.shape[1], 2))
    zz = np.full([2,2], 0.9)
    ax.plot_surface(xx, yy, zz, color = 'r', alpha = 0.2)
    ax.set_xlabel('Offset in x (px)')
    ax.set_ylabel('Offset in y (px)')
    ax.set_zlabel('Correlation score (normalised)')
    # plt.savefig('failed.png')
    plt.clf()

    from mpl_toolkits.axes_grid1 import make_axes_locatable
    fig = plt.figure()
    ax = fig.add_subplot()
    ax.set_xlabel('Offset in x (px)')
    ax.set_ylabel('Offset in y (px)')
    im = ax.imshow(corr, cmap = 'gray')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)

    fig.colorbar(im, cax=cax, orientation='vertical')
    # plt.savefig('2d_fail.png')
    plt.show(block=True)

def correlation_coefficient(patch1, patch2):
    if np.product(patch1.shape) == 0 or np.product(patch2.shape) == 0:
        return 0
    product = np.mean((patch1 - patch1.mean()) * (patch2 - patch2.mean()))
    stds = patch1.std() * patch2.std()
    if stds == 0:
        return 0
    else:
        product /= stds
        return product

def mse(imageA, imageB):
	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
    if float(imageA.shape[0] * imageA.shape[1]) < 1 or imageA.shape[0] < 4 or imageB.shape[0] < 4 or imageB.shape[1] < 4 or imageA.shape[1] < 4:
        return 9999
    else:
        imageA = cv2.resize(imageA, (0,0), fx = 1/4, fy = 1/4)
        imageB = cv2.resize(imageB, (0,0), fx = 1/4, fy = 1/4)
        # err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
        # err /= float(imageA.shape[0] * imageA.shape[1])**2 # this normalises for overlap size; bigger overlap with same error per pixel is better

        err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 4) # this punishes very different pixels more aggressively
        err /= float(imageA.shape[0] * imageA.shape[1])**2 # this normalises for overlap size; bigger overlap with same error per pixel is better
        return err
        # return the MSE, the lower the error, the more "similar"
        # the two images are


def quadrant_slices(split_point, quadrant):
    """XY slices to select a quadrant of an image
    
    This function returns a tuple of two slices, which will select
    one quadrant of an image, with its corner at `split_point`.
    
    `split_point` should be a 2 element array of integers between 0 and 
    the image size.
    
    `quadrant` should be a 2 element array of booleans. `False` means
    we take the part of the image starting at `split_point` until the
    end of the axis - `True` will take the image from 0 up to 
    `split_point`.
    
    If either element of `split_point` is negative, we flip the corresponding
    `quadrant` element - so `quadrant_slices([-100, 100], [False, False])`
    will return `[slice(None, -100), slice(100, None)]`.
    """
    slices = []
    for sp, negative in zip(split_point, quadrant):
        if sp < 0:
            # If the split point <0 we will be indexing from the end of
            # the axis - so we flip the quadrant as well.
            negative = not negative
        if negative:
            slices.append(slice(None, sp))
        else:
            slices.append(slice(sp, None))
    return tuple(slices)


def break_ties(overlay_image, base_image, peak_loc, plot = False):
    """Evaluate the correlation of two images at four positions, to break FFT ambiguity"""
    # Ensure peak_loc is a 2-element array, between 0 and the image shape
    # peak_loc may be negative - if that's the case we flip it round
    size = np.array(base_image.shape[:2])
    peak_loc = np.array(peak_loc, dtype=int) % size
    candidates = []
    # Every FFT correlation corresponds to four possible values - except if either
    # element is zero, at which point there's no ambiguity.
    # The comprehension below sets the quadrants we need to investigate.
    # `False` means a positive shift, `True` means a shift of `peak_loc[i]-size[i]`.
    quadrants = [
        [x_negative, y_negative]
        for x_negative in ([True, False] if peak_loc[0] != 0 else [False])
        for y_negative in ([True, False] if peak_loc[1] != 0 else [False])
    ]
    for quadrant in quadrants:
        score = correlation_coefficient(
            base_image[quadrant_slices(peak_loc, quadrant)], 
            overlay_image[quadrant_slices(-peak_loc, quadrant)]
        )
        candidates.append(
            {
                "score": score,
                "loc": peak_loc - size * np.array(quadrant),
                "quadrant": quadrant,
            }
        )

    best_candidate = np.argmax([c["score"] for c in candidates])

    if plot:
        fig, axs = plt.subplots(2,2)
        for c in candidates:
            quadrant = c["quadrant"]
            combined = base_image.copy()
            combined[quadrant_slices(peak_loc, quadrant)] = (
                overlay_image[quadrant_slices(-peak_loc, quadrant)]
            )
            axs[int(quadrant[0]), int(quadrant[1])].imshow(combined, cmap = 'gray')
            axs[int(quadrant[0]), int(quadrant[1])].set_xlabel(c["score"])
        plt.show(block=True)

    return candidates[best_candidate]["loc"]

def plot_overlaps(pairs, pair_data, pixel_positions,block=True):
    for h, (i, j) in enumerate(pairs):
        
        original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])

        data = pair_data['pairs'][f'{i}, {j}']

        transform = np.array((np.array(data['peak loc']) - original_displacement))
        plt.scatter(data['0.9 thresh'], np.sum(np.abs(transform)))
    plt.xlabel('Peak quality (higher is better)')
    plt.ylabel('Stage - correlation discrepancy /px \n (lower is better)')
    plt.tight_layout()
    # plt.savefig('scatter.png')
    plt.pause(0.1)
    plt.show(block=block)

def verify_overlaps(pairs, pair_data, pixel_positions, x_thresh, y_thresh):

    correlated_pixel_displacements = []
    truly_overlapping_pairs = []

    for h, (i, j) in enumerate(pairs):

        data = pair_data['pairs'][f'{i}, {j}']

        transform = data['transform']

        thresh_cutoff = x_thresh
        dist_cutoff = y_thresh

        if data['0.9 thresh'] < thresh_cutoff or np.sum(np.abs(transform)) > dist_cutoff or np.sum(np.abs(data['peak loc'])) < 1:
            pass
        else:
            correlated_pixel_displacements.append(data['peak loc'])
            truly_overlapping_pairs.append(pairs[h])

    correlated_pixel_displacements = np.array(correlated_pixel_displacements)

    return correlated_pixel_displacements, truly_overlapping_pairs

def ransac_pairs(correlated_pixel_displacements, overlapping_pairs, stage_displacements):
    remove_pairs = []
    remove_displacements = []

    for i in range(0, 2):
        X = stage_displacements[:,i].reshape(-1,1)
        y = correlated_pixel_displacements[:,i]

        # Fit line using all data
        lr = linear_model.LinearRegression()
        lr.fit(X, y)

        # Robustly fit linear model with RANSAC algorithm
        ransac = linear_model.RANSACRegressor()#residual_threshold=500*resize)
        ransac.fit(X, y)
        inlier_mask = ransac.inlier_mask_
        outlier_mask = np.logical_not(inlier_mask)

        # Predict data of estimated models
        line_X = np.arange(X.min(), X.max())[:, np.newaxis]
        line_y = lr.predict(line_X)
        line_y_ransac = ransac.predict(line_X)

        # Compare estimated coefficients
        print("Estimated coefficients (linear regression, RANSAC):")
        print(lr.coef_, ransac.estimator_.coef_)

        lw = 2
        plt.scatter(
            X[inlier_mask], y[inlier_mask], color="black", marker=".", label="Inliers"
        )
        plt.scatter(
            X[outlier_mask], y[outlier_mask], color="red", marker=".", label="Outliers"
        )
        plt.plot(line_X, line_y, color="navy", linewidth=lw, label="Linear regressor")
        plt.plot(
            line_X,
            line_y_ransac,
            color="cornflowerblue",
            linewidth=lw,
            label="RANSAC regressor",
        )

        remove_pairs.append(overlapping_pairs[~inlier_mask,:])
        remove_displacements.append(correlated_pixel_displacements[~inlier_mask,:])
        
        plt.legend()
        plt.xlabel("Stage recorded offset")
        plt.ylabel("Correlation offset")
        # plt.savefig(f'ransac_{i}.png', dpi = 800)
        # plt.show()

    remove_pairs = [item for sublist in remove_pairs for item in sublist]
    remove_displacements = [item for sublist in remove_displacements for item in sublist]

    overlapping_pairs = overlapping_pairs.tolist()
    correlated_pixel_displacements = correlated_pixel_displacements.tolist()

    for pair in remove_pairs:
        try:
            overlapping_pairs.remove(list(pair))
        except:
            pass

    for displacement in remove_displacements:
        try:
            correlated_pixel_displacements.remove(list(displacement))
        except:
            pass

    overlapping_pairs = np.array(overlapping_pairs)
    correlated_pixel_displacements = np.array(correlated_pixel_displacements)
    return overlapping_pairs, correlated_pixel_displacements


def pair_displacements(pairs, positions):
    """Calculate the displacement between each pair of positions specified.
    
    Arguments:
    pairs: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    
    Result: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    """
    return np.array([positions[j,:]-positions[i,:] for i,j in pairs])

                                            
def fit_affine_transform(pairs, positions, displacements):
    """Find an affine transform to make positions match a set of displacements.
    
    Find an affine tranform (i.e. 2x2 matrix) that, when applied to the 
    positions, matches the specified pair displacements as closely as possible.
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    
    Result: numpy.ndarray, numpy.ndarray
        A tuple of two things: firstly, a 2x2 matrix that transforms the given
        pixel positions to match the displacements.  Secondly, the positions
        so transformed (i.e. the corrected positions).
    """

    starting_displacements = pair_displacements(pairs, positions)

    affine_transform = np.linalg.lstsq(starting_displacements, 
                                       displacements, rcond=-1)[0]

    corrected_positions = np.dot(positions, affine_transform)
    
    return affine_transform, corrected_positions
    
def apply_affine_to_unknown(pairs, positions, starting_positions, affine_transform):
    """Find an affine transform to make positions match a set of displacements.
    
    Apply the affine transform to images that don't have any overlaps to estimate
    their position. Will only be a rough fit, but will position them in the right
    grid position, just not with ideal overlaps
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    starting_positions: numpy.ndarry

    
    Result: numpy.ndarray, numpy.ndarray
        A tuple of two things: firstly, a 2x2 matrix that transforms the given
        pixel positions to match the displacements.  Secondly, the positions
        so transformed (i.e. the corrected positions).
    """
    corrected_positions = []

    for count, position in enumerate(positions):
        if any(count in sl for sl in pairs):
            corrected_positions.append(position)
        else:
            corrected_positions.append(np.dot(starting_positions[count], affine_transform))

    return np.array(corrected_positions)


def rms_error(pairs, positions, displacements, print_err=False):
    """Find the RMS error in image positons (against some given displacements)
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    print_err: bool
        If true, print the RMS error to the console.
    
    Returns: float
        The RMS difference between the displacements calculated from the given
        positions and the displacements specified.
    """
    # with np.errstate(divide='raise'):
    #     try:
    error = np.std(pair_displacements(pairs, positions) - displacements, ddof=2*positions.shape[0])
        # except:
        #     error = 0
    if print_err:
        print ("RMS Error: %.2f pixels" % error)
    return error
        

###################### Look at each image and shift it to the optimal place ###
def pairs_per_image(pairs, positions):
    """Work out which pairs relate to each image
    
    This returns a list with one element per image. Each list element is a
    tuple of two numpy arrays, one of which contains indices (in the pairs
    or displacements array) of pairs that include the image. The second array
    is +1 if the image in question is the first one in the pair, or -1 if it
    is the second one.
    
    Using the pairs-indexed-by-image speeds up the position optimisation code.
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.  NB this
        will be modified!  Copy it first if you don't want that.
    """
    ret = []
    for k in range(positions.shape[0]): #for each image
        # find the displacements that include this image
        pair_indices = np.array([
            i for i, (a,b) in enumerate(pairs)
            if a==k or b==k
        ], dtype=int)
        pair_directions = np.array([
            1 if a==k else -1
            for i, (a,b) in enumerate(pairs)
            if a==k or b==k
        ])
        ret.append((pair_indices, pair_directions))
    return ret

def optimise_positions(pairs_by_image, pairs, positions, displacements):
    """Adjust the positions slightly so they better match the displacements.
    
    After fitting an affine transform (which takes out calibration error) we 
    run this method: it moves each tile to minimise the difference between 
    measured and current displacements.
    
     Arguments:
    pairs_by_image: [(np.array,np.array)]
        A list of N tuples, each of which contains an array of indices and
        an array of directions, returned by `pairs_per_image`.
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.  NB this
        will be modified!  Copy it first if you don't want that.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    
    Returns: numpy.ndarray
        The modified positions array.
    
    Side Effects:
        "positions" will be modified so it better matches displacements.
    """
    positions_d = pair_displacements(pairs, positions)
    for k, (indices, directions) in enumerate(pairs_by_image): #for each particle
        # find the displacement from here to each overlapping image
        if len(indices) == 0:
            pass
            # print(f'no overlaps found for tile {k}')
        # shift the current tile in the direction suggested by measured 
        # displacements
        else:
            # displacements from this image to its overlaps, measured by correlations
            measured_d = displacements[indices] * directions[:, np.newaxis]
            # displacements as we currently have them in `positions`
            current_d = positions_d[indices] * directions[:, np.newaxis]
            positions[k,:] -= np.mean(measured_d-current_d,axis=0) * 0.5
    return positions


def fit_positions_lstsq(pairs, displacements, return_A_matrix=False):
    """Find the least squares solution for image placement.
    
    Placing N images such that the pair-wise displacements match those measured
    can be formulated as a linear least squares problem, because for each 
    measured displacement, it can be written as a dot product between a vector
    with exactly two nonzero elements (one +1 and one -1) and the position 
    vector.

    This function calculates a matrix A such that each row of A is such a vector,
    and then uses that, together with the measured displacements, to recover the
    positions. In order to make sure the problem is constrained, we add an 
    extra two rows, constraining the mean position to be zero. We could trivially
    modify this to set an arbitrary mean position.
    
    Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    
    Returns: numpy.ndarray
        The optimal positions array.
    """
    
    N = np.max(pairs) + 1  # The highest index plus one gives #images
    M = len(pairs)

    # Our A matrix will have 2(M+1) rows, x and y constraints for each pair, plus x and y means
    d = np.concatenate([
        np.reshape(displacements, 2*M), 
        np.array([0,0]),
    ])
    A = np.zeros((2*M + 2, 2*N), dtype=float)
    # The last two rows sum up the x and y coordinates respectively
    for i in range(2):
        A[2*M + i, i::2] = 1/N
    # For the other rows, we take the difference between pairs of images
    for i, (a, b) in enumerate(pairs):
        for j in range(2):  # j selects x then y
            A[2*i + j, 2*a + j] = -1
            A[2*i + j, 2*b + j] = 1

    if return_A_matrix:
        return A

    # Now, solve the least squares problem
    positions, _ssr, _rank, _singular = np.linalg.lstsq(A, d, rcond=None)
    return positions.reshape((N, 2))


def optimise_positions_old(pairs, positions, displacements):
    """Adjust the positions slightly so they better match the displacements.
    
    After fitting an affine transform (which takes out calibration error) we 
    run this method: it moves each tile to minimise the difference between 
    measured and current displacements.
    
     Arguments:
    pairs: [(int,int)]
        A list of M tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.  NB this
        will be modified!  Copy it first if you don't want that.
    displacements: numpy.ndarray
        An Mx2 array, giving the 2D displacement between each pair of images.
    
    Returns: numpy.ndarray
        The modified positions array.
    
    Side Effects:
        "positions" will be modified so it better matches displacements.
    """
    positions_d = pair_displacements(pairs, positions)
    for k in range(positions.shape[0]): #for each particle
        # find the displacement from here to each overlapping image
        measured_d = np.array([d if i==k else -d 
                               for (i,j), d in zip(pairs, displacements)
                               if i==k or j==k])
        current_d = np.array([d if i==k else -d 
                               for (i,j), d in zip(pairs, positions_d)
                               if i==k or j==k])
        if len(measured_d) == 0:
            pass
            # print(f'no overlaps found for tile {k}')
        # shift the current tile in the direction suggested by measured 
        # displacements
        #print "shift:", np.mean(measured_d-current_d,axis=0)
        else:
            positions[k,:] -= np.mean(measured_d-current_d,axis=0) * 0.5
    return positions

def plot_pairs(peak, tiles, i, j, colour, dpi):

    img_i = cv2.imread(tiles[i], -1)
    # img_j = cv2.imread(tiles[j], -1)

    # fig, axs = plt.subplots(2,2)
    # axs[0, 0].imshow(img_i)
    # axs[0, 0].scatter(peak[1], peak[0], edgecolors = 'red', facecolors = 'none')
    # axs[0, 1].imshow(img_j)
    # # axs[1, 0].imshow(corr)
    # # axs[1, 0].scatter(peak[1], peak[0], edgecolors = 'red', facecolors = 'none')

    # # axs[1, 0].set_xlabel(under_thresh / np.count_nonzero(corr))
    # peak = np.asarray(peak).astype(int)

    # canvas = np.zeros([int(abs(peak[0]) + img_i.shape[0]), int(abs(peak[1]) + img_i.shape[1]), 3])
    # if peak[1] >= 0:
    #     if peak[0] >= 0 :
    #         canvas[:img_i.shape[0], :img_i.shape[1]] = img_i
    #         canvas[peak[0]:peak[0]+img_i.shape[0], peak[1]:peak[1]+img_i.shape[1]] = img_j
    #     else:
    #         canvas[-peak[0]:-peak[0]+img_i.shape[0], :img_i.shape[1]] = img_i
    #         canvas[:img_i.shape[0],peak[1]:peak[1]+img_i.shape[1]] = img_j
    # else:
    #     if peak[0] >= 0 :
    #         canvas[:img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
    #         canvas[peak[0]:peak[0]+img_i.shape[0], :img_i.shape[1]] = img_j
    #     else:
    #         canvas[-peak[0]:-peak[0]+img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
    #         canvas[:img_i.shape[0], :img_i.shape[1]] = img_j
    # axs[1,1].imshow(canvas.astype(np.uint8))
    # # plt.show()
    # fig.savefig('pairs\{}_{}.png'.format(i, j), dpi = dpi, facecolor = colour)
    # plt.close(fig)

    # plt.imshow(corr)
    # print(under_thresh / np.count_nonzero(corr))
    # plt.savefig(f"{i}_{j}_corr.png")

class PathException(Exception):
    pass

def pull_timestamps(img_list, folderpath):
    timestamps = []
    
    for i in img_list:
        data = pull_usercomment_dict(os.path.join(folderpath, i))
        timestamps.append(data['image']['time'])

    return timestamps

def reconstruct_tiled_image(dir,
                            img_list,
                            camera_to_sample=np.identity(2),
                            resize=0.6,
                            unknown_images = "warn",
                            fractional_overlap = 0.3,
                            img_limit = 0,
                            pad = False,
                            use_cache=True,
                            priority='time'):
    """Combine a sequence of images into a large tiled image.
    
    This function takes a list of images and approximate positions.  It first
    aligns the images roughly, then uses crosscorrelation to find relative
    positions of the images.  Positions of the images are optimised and the
    tiles are then stitched together with a crude pick-the-closest-image-centre
    algorithm.  Importantly, we keep careful track of the relationship between
    pixels in the original images, their positions (in "sample" units), and the
    same features in the stitched image.
    
    Arguments:
    folder: str
        The folder containing images to be tiled. Each image should have
        OpenFlexure metadata containing its stage position and the 
        Camera Stage Mapping matrix to estimate its pixel position.
    camera_to_sample: numpy.ndarray
        The camera_to_sample matrix maps displacement in camera units (where 
        the width or height of an image is 1) into sample units (generally
        corresponding to actual distance).
    downsample: int
        Downsampling factor (produces a less huge output image).  Only applies
        to the final stitching step.
    resize: float
        Resizing factor for the calculations and positioning, but not the
        final image. Higher is slower but likely more accurate
    unknown_images: str
        How to treat tiles with images that don't appear to overlap.
        If "break", return an error message
        If "ignore", attempt to position this images roughly in line with
        the estimated affine transform
        If "warn", behave like "ignore", but print a warning to the console
    
    Returns: dict
        This function returns a dictionary with various elements:
        stitched_image: numpy.ndarray
            The stitched image as a numpy array
        stitched_to_sample: numpy.ndarray
            A mapping matrix that transforms from image coordinates (0-1 for X 
            and Y) to "sample" coordinates (whatever the original positions 
            were specified in)
        stitched_centre: numpy.ndarray
            The centre of the stitched image, again in sample coordinates.
        image_centres: numpy.ndarray
            The centres of the tiles, in pixels in the stitched image (mostly 
            for debug purposes, but can be useful to locate the original tile 
            for a given pixel).  It has dimensions Nx2, where N is the number
            of source images.
        corrected_camera_to_sample: numpy.ndarray
            The 2x2 mapping matrix passed as input, tweaked to better match
            the measured displacements between the images, and their given
            positions.
    """

    try:
        if not use_cache:
            raise Exception
        use_cache = False

        with open(os.path.join(dir, 'export.pkl'), 'rb') as f:
            imported = pickle.load(f)
        
        use_cache = True

        if img_limit !=0:
            img_list = img_list[:img_limit]
        
        if 'pairs' not in list(imported.keys()) or 'timestamps' not in list(imported.keys()) or 'CSM' not in list(imported.keys()):
            use_cache = False
            print('Cache has different shape')
            raise Exception
        
        try:
            first_pair = list(imported['pairs'].keys())[0]
            x = imported['pairs'][first_pair]['transform']
        except:
            print('Not cached transforms')
            use_cache = False
            raise Exception

        try:
            if pad != imported['pad']:
                print('Pad settings changed')
                use_cache = False
                raise Exception
        except:
            use_cache = False
            print('No previous pad settings')
            raise Exception

        timestamps = pull_timestamps(img_list, dir)

        if not np.array_equal(imported['timestamps'], timestamps):
            use_cache = False
            print('Image timestamps changed')
            raise Exception
        
        if not np.array_equal(imported['CSM'], camera_to_sample):
            use_cache = False
            print('CSM changed')
            raise Exception
        
        if not np.array_equal(imported['resize'], resize):
            use_cache = False
            print(f'Resize changed from {imported["resize"]} to {resize}')
            raise Exception
        try:
            if not np.array_equal(imported['fractional_overlap'], fractional_overlap):
                use_cache = False
                print(f'Fractional overlap limit changed from {imported["fractional_overlap"]} to {fractional_overlap}')
                raise Exception
        except:
            use_cache = False
            raise Exception
        
        if len(img_list) != imported['img_count']:
            print('Image count has changed')
            raise Exception
        
        if not os.path.isfile(os.path.join(dir, 'metadata.pkl')):
            print('No metadata cache')
            raise Exception

        tiles = [os.path.join(dir, i) for i in img_list]

        pair_data = imported.copy()
        scan_centre = pair_data['scan_centre']
        pairs = [[int(i.replace(',', '').split(' ')[0]), int(i.replace(',', '').split(' ')[1])] for i in list(pair_data['pairs'].keys())]
        
        pixel_positions = pair_data['pixel_positions']
        starting_pixel_positions = pair_data['pixel_positions']
    
        print('Cache successful, skipping to thresholding')

    except Exception as e:
        print(e)
        tiles, pixel_positions, scan_centre, stage_positions, timestamps = get_pixel_positions(dir, img_list, camera_to_sample, resize, img_limit)

        image, _, _ = stitch_images(tiles, pixel_positions / resize, downsample= 9, method="latest")

        cv2.imwrite(os.path.join(dir, "stage_coords_image.jpg"), cv2.cvtColor(image[:,:,::-1], cv2.COLOR_BGR2RGB))

        starting_pixel_positions = pixel_positions.copy()
        
        first_img = cv2.resize(cv2.imread(tiles[0], -1), dsize = [0,0], fx = resize, fy = resize)
        image_size = np.array(first_img.shape[:2])
        
        # then find images estimated to have at least fractional_overlap in common
        pairs = find_overlapping_pairs(pixel_positions, image_size, fractional_overlap)

        print('Cache failed, redoing overlaps (may take some time)')

        all_sites = sites_linked(pairs, len(tiles))

        if all_sites is False:
            if unknown_images == "break":
                raise PathException("Not all sites overlap enough according to your inputs, check your images and update the CSM or overlap")
            elif unknown_images == "warn":
                print("Not all sites overlap enough according to your inputs, continuing")

        pair_data = {}

        if use_cache:
            try:
                with open(os.path.join(dir, 'export.pkl'), 'rb') as f:
                    cached_pair_data = pickle.load(f)
            except:
                print('There is no cache to import')            
                cached_pair_data = {}
        else:
            cached_pair_data = {}

        metadata = pull_usercomment_dict(tiles[0])
        del metadata['image']
        with open(os.path.join(dir, 'metadata.pkl'), 'wb') as f:
            pickle.dump(metadata, f)

        # compare overlapping images to find the true displacement between them
        pair_data['pairs'] = crosscorrelate_overlapping_images(tiles, pairs, pixel_positions, 
                                                        pad = pad, resize = resize, cache = cached_pair_data, priority=priority)

        pair_data['pixel_positions'] = pixel_positions

        pair_data['resize'] = resize
        pair_data['CSM'] = camera_to_sample
        pair_data['timestamps'] = timestamps
        pair_data['img_count'] = len(tiles)
        pair_data['scan_centre'] = scan_centre
        pair_data['pad'] = pad
        pair_data['fractional_overlap'] = fractional_overlap

        with open(os.path.join(dir, 'export.pkl'), 'wb') as f:
            pickle.dump(pair_data, f)

    return pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre

def stitch_from_thresholds(dir, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, x_thresh, y_thresh, img_limit, downsample):
    start = time.time()
    displacements, pairs = verify_overlaps(pairs, pair_data, pixel_positions, x_thresh, y_thresh)

    pairs = np.array(pairs)

    all_sites = sites_linked(pairs, len(tiles))
    if all_sites is False:
        if unknown_images == "break":
            raise PathException("Not all images overlap, try lowering cross correlate theshold")
        elif unknown_images == "warn":
            print("Not all sites overlap enough according to your inputs, continuing")

    # now we start the optimisation...
    errors = [rms_error(pairs, pixel_positions, displacements, print_err=True)]


    # first, fit an affine transform, to correct for the calibration between
    # camera and stage being slightly off (rotation, scaling, etc.)
    affine_transform, pixel_positions = fit_affine_transform(pairs, pixel_positions,
                                                       displacements)

    errors.append(rms_error(pairs, pixel_positions, displacements, print_err=False))

    print("Optimising image positions...")
    # next, optimise the positions iteratively
    pairs_by_image = pairs_per_image(pairs, pixel_positions)
    while (errors[-2] - errors[-1]) > 0.001: #len(errors) < 5 or
        pixel_positions = optimise_positions(pairs_by_image, pairs, pixel_positions, displacements)
        errors.append(rms_error(pairs, pixel_positions, displacements, print_err=False))
        if len(errors) > 500:
        # if len(errors) > 100:
            break

    # plt.plot(errors,'.')
    # plt.show()

    rms_error(pairs, pixel_positions, displacements, print_err=True)
    
    print(f'Time to adjust threshold is {time.time() - start}')

    print(np.std(np.sqrt(np.sum(np.square(pair_displacements(pairs, pixel_positions) - displacements), axis=0))))

    print(f"Recalculating displacements with least squares")

    pixel_positions = fit_positions_lstsq(pairs, displacements)
    rms_error(pairs, pixel_positions, displacements, print_err=True)
    print(np.std(np.sqrt(np.sum(np.square(pair_displacements(pairs, pixel_positions) - displacements), axis=0))))
    

    # print(pair_displacements(pairs, pixel_positions) - displacements)

    tweak_affine_transform, _ = fit_affine_transform(pairs, pixel_positions,
                                                       displacements)

    improved_affine_transform = np.dot(affine_transform, tweak_affine_transform)

    try:
        corrected_camera_to_sample = np.dot(np.linalg.inv(affine_transform),
                                            camera_to_sample)
        improved_corrected_camera_to_sample = np.dot(tweak_affine_transform,
                                            corrected_camera_to_sample)
        # print(f"Input is {camera_to_sample}")                                    
        # print(f"First loop is {corrected_camera_to_sample}")
        # print(f"Final loop is {np.array2string(improved_corrected_camera_to_sample, separator=',')}")
    except:
        improved_corrected_camera_to_sample = 0
        print('Singular matrix, can\'t invert')

    # print(f'pairs are {pairs}')

    pixel_positions = apply_affine_to_unknown(pairs, pixel_positions, starting_pixel_positions, improved_affine_transform)

    np.savez(os.path.join(dir, 'scan_settings'), positions=pixel_positions, camera_to_stage=camera_to_sample, resize=resize, unknown_images=unknown_images, fractional_overlap= fractional_overlap, img_limit=img_limit, downsample = downsample, x_thresh = x_thresh, y_thresh=y_thresh)

    return tiles, pixel_positions, improved_corrected_camera_to_sample, scan_centre

def generate_stitched_image(tiles, positions, resize, downsample, csm, scan_centre):
    
    first_img = cv2.resize(cv2.imread(tiles[0], -1), dsize = [0,0], fx = resize, fy = resize)
    image_size = np.array(first_img.shape[:2])

    print("Combining images...")
    
    # plot_images_in_situ(tiles, positions/resize, None, 9, True, True)
    
    stitched_image, stitched_centre, image_centres = stitch_images(tiles, 
                                            positions/resize, downsample=downsample, method="latest")

    # now work out how the new image relates to the stage coordinates
    # the correct camera-to-sample matrix for source images is 
    # corrected_camera_to_sample; we need to adjust for image size though.
    
    stitched_size = np.array(stitched_image.shape[:2])
    size_in_images =  stitched_size.astype(float) * downsample / image_size
    stitched_to_sample = csm * size_in_images[:, np.newaxis]
    
    shift_in_sample_coords = np.dot(stitched_centre/image_size,
                                    csm)
    stitched_centre = np.append(shift_in_sample_coords,0)[:2] + scan_centre[:2]

    return {'stitched_image': stitched_image, 
            'stitched_to_sample': stitched_to_sample, 
            'stitched_centre': stitched_centre, 
            'image_centres': image_centres,
            'corrected_camera_to_sample': csm}


def sites_linked(pairs, sites, print_res = True):
    # start = time.time()

    if len(pairs) == 0:
        return False

    sites_in_chain = [pairs[0][0]]

    cont = True

    while cont == True:
        new_sites_in_chain = sites_in_chain
        for site in new_sites_in_chain:
            for pair in pairs:
                if site in pair:
                    if pair[0] not in new_sites_in_chain:
                        new_sites_in_chain.append(pair[0])
                    elif pair[1] not in new_sites_in_chain:
                        new_sites_in_chain.append(pair[1])
        if sites_in_chain == new_sites_in_chain:
            cont = False

    pair_elements = set(range(sites))
    chain_elements = set(new_sites_in_chain)

    if pair_elements != chain_elements and print_res:
        differences = []
        for site in pair_elements:
            if site not in chain_elements:
                differences.append(site)
        if print_res:
            print(f'The images not in the conjoined path are {differences}')
    # print(time.time()-start)
    return pair_elements == chain_elements
