import os
from openflexure_stitching.pipeline import load_tile_and_stitch_cli
from openflexure_stitching.stitching.find_vips import find_vips_and_add_to_path

if __name__ == "__main__":
    vips_paths = [".", os.path.split(__file__)[0]]
    print(f"Searching {vips_paths}")
    find_vips_and_add_to_path(vips_paths)
    load_tile_and_stitch_cli()
