from openflexure_stitching.pipeline import load_tile_and_stitch

load_tile_and_stitch(
    "./datasets/97/",
    plot_inputs=True,
    plot_correlations=False,
    stitch_from_stage=False,
    stitch_from_correlations=False,
)