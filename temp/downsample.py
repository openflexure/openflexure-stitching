import numpy as np
import cv2
import os
import sys


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            images.append(filename)
    return images

def downsample_images(folderpath, images, factor):
    images = load_images_from_folder(folderpath)

    for i in images:
        img = cv2.imread(os.path.join(folderpath,i))
        img = cv2.resize(img, dsize = (0,0), fx= 0.5, fy = 0.5)
        cv2.imwrite(os.path.join(folderpath, 'downsampled', i), img)

if __name__ == "__main__":
    folderpath = sys.argv[1]
    
    imgs = load_images_from_folder(folderpath)

    os.makedirs(os.path.join(folderpath, 'downsampled'), exist_ok=True)

    downsample_images(folderpath, imgs, 0.5)