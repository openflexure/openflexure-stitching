from typing import List
from openflexure_stitching.loading import find_images, load_metadata_for_images, tiling_inputs_from_metadata, lookup_pairs_from_cache, save_pairs_to_cache
from openflexure_stitching.correlation import find_overlapping_pairs, crosscorrelate_overlapping_images
from openflexure_stitching.types import CorrelationSettings
from openflexure_stitching.plotting import plot_overlaps
from openflexure_stitching.optimisation import test_all_thresholds, filter_pairs, fit_positions_lstsq
from openflexure_stitching.stitching import stitch_images
import matplotlib.pyplot as plt
import numpy as np
import cv2
import os

if __name__ == "__main__":
    folder = r"./datasets/97/"
    resize = 1
    correlation_settings = CorrelationSettings(pad=True)

    fnames = find_images(folder)
    image_metadata = load_metadata_for_images(folder, fnames)
    tiling_inputs = tiling_inputs_from_metadata(image_metadata, folder)

    stage_stitched, _mean, _centres = stitch_images(
        tiles = [i.filename for i in image_metadata],
        positions = [i.position_from_stage for i in tiling_inputs.images],
        downsample = 3,
    )
    cv2.imwrite(os.path.join(folder, "stage_coords_image.jpg"), stage_stitched[:,:,:])

    overlapping_pairs = find_overlapping_pairs(tiling_inputs, fractional_overlap=0.2)
    pairs, pairs_to_correlate = lookup_pairs_from_cache(
        folder_path = folder,
        settings = correlation_settings,
        image_metadata = image_metadata,
        overlapping_pairs = overlapping_pairs
    )
    pairs += crosscorrelate_overlapping_images(
        inputs = tiling_inputs,
        overlapping_pairs = pairs_to_correlate,
        settings = correlation_settings
    )
    save_pairs_to_cache(
        folder_path = folder, 
        settings = correlation_settings, 
        image_metadata = image_metadata,
        pair_data = pairs,
    )
    #f = plot_overlaps(pairs)
    #plt.show(block=True)
    x_thresh, y_thresh = test_all_thresholds(pairs)
    filtered_pairs = filter_pairs(pairs, x_thresh, y_thresh)
    positions = fit_positions_lstsq(filtered_pairs)
    stitched, mean, centres = stitch_images(
        tiles = [i.filename for i in image_metadata],
        positions = positions,
        downsample = 1,
    )
    cv2.imwrite(os.path.join(folder, "stitched_image.jpg"), stitched[:,:,:])
    
    #print(f"Pair data:")
    #for p in pairs:
    #    print(f"{p.indices}: corr {np.round(p.image_displacement)}, stage {np.round(p.stage_displacement)}, {'excluded' if p not in filtered_pairs else ''}")
