"""
Manage metadata associated with images

This tiling system currently expects images to have metadata in the
format used by OpenFlexure (a JSON string in the usercomment EXIF
field). It would be lovely to generalise this!
"""

from datetime import datetime
import json
import numpy as np
import os
import re
import piexif
import cv2
from PIL import Image

from typing import Dict, List, Any
from ..types import ImageMetadata

class DataException(Exception):
    '''Raise this is there's some metadata that we need that can't be loaded or guessed'''
    pass

def read_metadata_from_file(fname: str) -> ImageMetadata:
    """Read the important metadata from an image file
    
    Currently this expects OpenFlexure-format metadata, which is stored
    as a JSON string in the UserComment EXIF field. In the future, we should
    extend this function to use other metadata standards such as OME-XML
    or OME-TIFF.
    """
    
    try:
        exif = piexif.load(fname)
    except:
        exif = None
    
    try:
        usercomment = exif_usercomment_json(exif)
    except:
        usercomment = None

    try:
        width, height = get_img_size(fname, exif)
    except:
        raise DataException()
    
    try:
        ctime = exif['0th'][piexif.ImageIFD.DateTime].decode()
        try:
            ctime = datetime.strptime(ctime, '%y:%m/%d %H:%M:%S')
        except:
            ctime = datetime.strptime(ctime, '%Y:%m:%d %H:%M:%S')
    except:
        # mtime works to get the creation time in windows, need to test linux
        ctime = os.path.getmtime(fname)

    try:
        pixel_size_um = usercomment['instrument']['settings']['extensions']['usafcal']['um_per_px']
    except (KeyError, TypeError) as e:
        pixel_size_um = 0
    
    try:
        camera_to_sample_matrix = np.array(
        usercomment['instrument']['settings']['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement']
        )
    except (KeyError, TypeError) as e:
        try:
            camera_to_sample_matrix = np.array(
                usercomment['/camera_stage_mapping/']['image_to_stage_displacement_matrix']
            )
        except (KeyError, TypeError) as e:
            camera_to_sample_matrix = None

    try:
        stage_position = [
            int(usercomment['instrument']['state']['stage']['position'][dim])
            for dim in ['x', 'y', 'z']
        ]
    except (KeyError, TypeError) as e:
        try:
            stage_position = [
                int(usercomment['/stage/']['position'][dim])
                for dim in ['x', 'y', 'z']
            ]
        except (KeyError, TypeError) as e:
            try:
                # Only check the tail (filename) for 2-3 indices, with optional - sign
                path = os.path.normpath(fname)
                filename = path.split(os.sep)[-1] #.split('.')[0]
                try:
                    m = re.match(".*(-?\d+)_(-?\d+)_(-?\d+)\..*", filename)

                    # The exception is only raised at this point, once m has no groups
                    stage_position = [int(d) for d in m.groups()]
                except:
                    try:
                        m = re.match(".*_(-?\d+)_(-?\d+)\..*", filename)
                        stage_position = [int(d) for d in m.groups()]
                    except:
                        x = int(filename.split('_')[0])
                        y = int((filename.split('_')[1]).split('.')[0])
                        stage_position = [x, y]
            except:
                print(f"Can't find location data for {filename}")
                raise DataException()   
    return ImageMetadata(
        filename = fname,
        camera_to_sample_matrix=camera_to_sample_matrix,
        timestamp = ctime,
        tags = [],
        stage_position = stage_position,
        width = width,
        height = height,
        pixel_size_um = pixel_size_um,
    )

def get_img_size(fname:str, metadata: Dict = None):
    if not metadata:
        try:
            metadata = piexif.load(fname)
        except:
            metadata = None
    try:
        try:
            width = metadata["Exif"][piexif.ExifIFD.PixelXDimension]
            height = metadata["Exif"][piexif.ExifIFD.PixelYDimension]
        except: 
            # Using opencv made this terrifyingly slow
            img=Image.open(fname)
            width = img.size[0]
            height = img.size[1]
        return width, height
    except:
        raise DataException()

def replace_csm(metadata_list: List[ImageMetadata], camera_to_sample_matrix: np.ndarray):
    "Replace the camera_to_sample_matrix (editing in-place)"
    for m in metadata_list:
        m.camera_to_sample_matrix = camera_to_sample_matrix

def load_csm_width(folder, fnames):
    fname = os.path.join(folder, fnames[0])
    try:
        exif = piexif.load(fname)
        usercomment = exif_usercomment_json(exif)
        csm_calibration_width = usercomment['/camera_stage_mapping/']['image_resolution'][1]
        return csm_calibration_width
    except:
        raise Exception

def scale_csm(metadata_list: List[ImageMetadata], calibration_width: int):
    "Account for a calibration width that may differ from image width"
    scale = metadata_list[0].width / calibration_width  # Usually >1, if we calibrated at low res
    csm = metadata_list[0].camera_to_sample_matrix / scale  # Decrease the CSM if pixels are smaller]
    replace_csm(metadata_list=metadata_list, camera_to_sample_matrix=csm)


def exif_usercomment_json(exif_dict: Dict[str, Any]) -> Dict:
    """Extract the OpenFlexure metadata from a usercomment dict"""
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        return json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
    else:
        raise KeyError("There is no EXIF UserComment on the image")