"""
Identify overlapping pairs of images, and cross-correlate them to find displacements
"""

import os
os.environ['MPLCONFIGDIR'] = "/tmp/"

import matplotlib.pyplot as plt
import cv2
import numpy as np
import psutil
from camera_stage_mapping.fft_image_tracking import (
    grayscale_and_padding,
    high_pass_fourier_mask,
    background_subtracted_centre_of_mass,
)
from typing import List, Dict, Optional
from ..types import (
    PairData, 
    PairIndices, 
    TilingInputs, 
    XYDisplacementInPixels, 
    CorrelationSettings,
)
import time

from ..loading.cache import lookup_pairs_from_cache, save_pairs_to_cache
from tqdm.auto import tqdm


def correlate_overlapping_images(
        tiling_inputs: TilingInputs,
        settings: CorrelationSettings,
    ) -> List[PairData]:
    """Determine overlapping images and find their offsets
    
    This is the top-level function that identifies overlaps based
    on the stage coordinates, and performs image correlation to
    determine relative displacements between images.
    
    We use a cache file to avoid recalculating image correlations
    if we want to change other tiling settings (like output format
    or optimisation of the image positions).

    The cache is "keyed" on the correlation settings, so if these
    change, correlations are re-done. Previous settings are still
    kept in the cache, so if you have used those settings before
    the correlations should still be found.
    """
    overlapping_pairs = find_overlapping_pairs(
        tiling_inputs, 
        fractional_overlap=settings.minimum_overlap
    )
    pairs, pairs_to_correlate = lookup_pairs_from_cache(
        tiling_inputs=tiling_inputs,
        settings = settings,
        overlapping_pairs = overlapping_pairs
    )
    pairs += crosscorrelate_images(
        inputs = tiling_inputs,
        overlapping_pairs = pairs_to_correlate,
        settings = settings
    )
    save_pairs_to_cache(
        tiling_inputs = tiling_inputs,
        settings = settings,
        pair_data = pairs,
    )
    return pairs


def find_overlapping_pairs(
        inputs: TilingInputs,
        fractional_overlap: float = 0.1,
    ) -> List[PairIndices]:
    """Identify pairs of images with significant overlap.
    
    Given the positions (in pixels) of a collection of images (of given size),
    calculate the fractional overlap (i.e. the overlap area divided by the area
    of one image) and return a list of images that have significant overlap.
    
    Arguments:
    pixel_positions: list of 2D positions
        An Nx2 array, giving the 2D position in pixels of each image.
    image_size: tuple of integers
        The size of each image in pixels.
    fractional_overlap: float
        The fractional overlap (overlap area divided by image area) that two
        images must have in order to be considered overlapping.
    
    Returns: List[PairIndices]
        A list of tuples, where each tuple describes two images that overlap.
    """
    tile_pairs: List[PairIndices] = []
    pixel_positions = np.asarray([i.position_from_stage for i in inputs.images])
    image_size = np.asarray(inputs.image_shape)
    for i in range(pixel_positions.shape[0]):
        for j in range(i+1, pixel_positions.shape[0]):
            overlap = image_size - np.abs(pixel_positions[i,:2] -
                                          pixel_positions[j,:2])
            overlap[overlap<0] = 0
            tile_pairs.append((i, j, np.prod(overlap)))
    overlap_threshold = np.prod(image_size)*fractional_overlap
    overlapping_pairs: List[PairIndices] = [
        (i,j) for i,j,o in tile_pairs 
        if o>overlap_threshold
    ]
    # Sanity checks for the output list of pairs
    for pair in overlapping_pairs:
        if pair[1] == pair[0]:
            raise AssertionError("An image has been linked to itself!")
        if pair[0] > pair[1]:
            raise AssertionError("A pair has been defined backwards!")

    return overlapping_pairs

class ImageAndFFTCache:
    """Cache images and FFTs to avoid too much re-reading from disk
    
    Currently, this class is all-or-nothing, i.e. it will either
    read each image exactly once, or every time it's needed. A more
    nuanced approach might reach a better compromise, e.g. limiting
    the number of images held in cache, or even intelligently
    planning the order of correlations to minimise the cache 
    required.

    TODO: check this change.
    A change from Joe's code is that I now cache images as well as
    FFTs, because `break_ties` needs original images. I'm also now
    running `break_ties` on resized images.
    """
    images: Dict[int, np.ndarray]
    ffts: Dict[int, np.ndarray]
    use_cache: bool = True
    def __init__(self, inputs: TilingInputs, settings: CorrelationSettings):
        self.inputs = inputs
        self.settings = settings
        self.use_cache = self.settings.priority=="time"
        self.images = {}
        self.ffts = {}

    def image(self, index: int) -> np.ndarray:
        """Load and resize an image"""
        try:
            return self.images[index]
        except KeyError:
            if self.settings.resize == 1.0:
                image = cv2.imread(self.inputs.images[index].filepath, -1)
            else:
                image = cv2.resize(
                    cv2.imread(self.inputs.images[index].filepath, -1),
                    dsize = [0,0],
                    fx = self.settings.resize,
                    fy = self.settings.resize
                )
                
        if self.use_cache and psutil.virtual_memory().available / (1024.0 ** 3) > 0.3:
            self.images[index] = image
        return image
    
    def fft(self, index: int) -> np.ndarray:
        """Find an initial FFT, either calculated or from cache."""
        try:
            return self.ffts[index]
        except KeyError:
            # This duplicates code in `camera_stage_mapping.high_pas_fft_template`
            image, fft_shape = grayscale_and_padding(
                self.image(index), 
                self.settings.pad
            )
            fft_image = np.fft.rfft2(image, s=fft_shape)
            # End of duplicated code

        if self.use_cache and psutil.virtual_memory().available / (1024.0 ** 3) > 0.3:
            self.ffts[index] = fft_image
        return fft_image
    
    def purge_images(self, retain: Optional[List[PairIndices]]) -> None:
        """Remove images not in the supplied list"""
        if retain:
            indices_to_retain = set(
                [i for i, _j in retain]
                + [j for _i, j in retain]
            )
        else:
            indices_to_retain = set()
        # NB the keys below are copied with list() to avoid errors due to changing
        # the keys during iteration because we delete elements.
        for i in list(self.images.keys()):
            if i not in indices_to_retain:
                del self.images[i]
        for i in list(self.ffts.keys()):
            if i not in indices_to_retain:
                del self.ffts[i]


def stage_displacement(inputs: TilingInputs, i: int, j: int) -> XYDisplacementInPixels:
    """Displacement in pixels from image i to j estimated from stage coords"""
    return (
        np.array(inputs.images[j].position_from_stage)
        - np.array(inputs.images[i].position_from_stage)
    )

def fraction_under_threshold(corr: np.ndarray) -> Dict[float, float]:
    """Measure the quality of a peak
    
    We use the fraction of the image below a threshold value as a measure of
    the quality of our correlation.
    
    Fractional thresholds are scaled such that 0 means the minimum value of
    the correlation image and 1 is the maximum. By default a single threshold
    of 0.9 is used.
    
    We return the fraction of nonzero pixels in the correlation image that
    are below the threshold, i.e. a greater number means a sharper peak.
    """
    fraction_under_threshold: Dict[float, float] = {}
    for thresh in [0.9]:  # previously [0.5, 0.75, 0.9, 0.95]:
        # The threshold is scaled such that 0 means min(corr) and
        # 1 means max(corr), and we count the fraction of pixels in
        # corr (the correlation result) beneath this value.
        # corr_size = np.count_nonzero(corr)  # TODO: why not just product(corr.shape)?
        corr_size = np.prod(corr.shape)
        abs_thresh = (np.min(corr) + thresh * (np.max(corr) - np.min(corr)))
        under_thresh = np.count_nonzero(corr < abs_thresh)
        fraction_under_threshold[thresh] = under_thresh / corr_size
    return fraction_under_threshold


def crosscorrelate_images(
        inputs: TilingInputs,
        overlapping_pairs: List[PairIndices],
        settings: CorrelationSettings,
    ) -> List[PairData]:
    """Calculate actual displacements between pairs of overlapping images.
    
    For each pair of overlapping images, perform a cross-correlation to
    fine-tune the displacement between them.
    
    Arguments:
    inputs: TilingInputs
        A list of datasets (or numpy images) that represent the images.
    overlapping_pairs: List[PairIndices]
        A list of tuples, where each tuple describes two images that overlap.
    
    Results: np.ndarray
        An Mx2 array, giving the displacement in pixels between each pair of
        images specified in overlapping_pairs.
    """
    pair_data: List[PairData] = []
    cache = ImageAndFFTCache(inputs, settings)
    failures = []
    
    # Load the first image, to determine size and allow us to calculate the Fourier filter applied to each correlation
    # assert cache.image(0).shape[:2] == inputs.image_shape  # Make sure there's no inconsistency here
    high_pass_filter = high_pass_fourier_mask(cache.fft(0).shape, settings.high_pass_sigma)
    for h, (i, j) in enumerate(overlapping_pairs):# enumerate(tqdm(overlapping_pairs, "Correlating overlapping images")):
        corr = np.fft.irfft2(np.conj(cache.fft(i)) * high_pass_filter * cache.fft(j))
        mean_corr = np.mean(corr)
        mask_width = 45
        corr[0:mask_width, 0:mask_width] = mean_corr
        corr[-mask_width:, 0:mask_width] = mean_corr
        corr[-mask_width:, -mask_width:] = mean_corr
        corr[0:mask_width, -mask_width:] = mean_corr
        trial_peak = background_subtracted_centre_of_mass(
            corr, fractional_threshold=0.02, quadrant_swap=True
        )
        
        # This is plotting some figures for the paper, lets the user visualise correlations
        # corr = corr - np.min(corr)
        # corr = corr / np.max(corr)
        # plt.clf()
        # plt.imshow(corr, cmap = 'gray')
        # plt.xlabel("x offset (px)",fontsize=18)
        # plt.ylabel("y offset (px)",fontsize=18)
        # cbar = plt.colorbar()
        # cbar.set_label('Normalised correlation \n', rotation = 270, labelpad=30,fontsize=18)
        # plt.tight_layout()
        # plt.savefig(f"plots/{i}_{j}_flat.png")
        
        # x = np.arange(corr.shape[0])
        # y = np.arange(corr.shape[1])
        # (y ,x) = np.meshgrid(x,y)
        # fig = plt.figure(figsize=plt.figaspect(0.5))
        # ax = fig.add_subplot(1, 1, 1, projection='3d')
        # ax.plot_surface(x,y,corr.T)
        
        # xx, yy = np.meshgrid(range(corr.shape[1]), range(corr.shape[0]))
        # zz = yy*0+0.9
        # ax.view_init(elev=20)
        # ax.plot_surface(xx, yy, zz, color='red', alpha = 0.4)
        # ax.set_xlabel("x offset (px)",fontsize=18)
        # ax.set_ylabel("y offset (px)",fontsize=18)
        # ax.set_zlabel("Normalised \n correlation",fontsize=18, labelpad=20)
        # fig.tight_layout()
        # plt.savefig(f'plots/{i}_{j}.png')
        # plt.close()

        # TODO: check if "resize" breaks this
        if not settings.pad:
            trial_peak = break_ties(
                cache.image(i),
                cache.image(j),
                np.unravel_index(np.argmax(corr), np.array(corr).shape), 
            )
        trial_peak *= -1

        pair_data.append(
            PairData(
                indices = (i, j),
                image_displacement = trial_peak / settings.resize,
                stage_displacement = stage_displacement(inputs, i, j),
                fraction_under_threshold = fraction_under_threshold(corr),
            )
        )

        del corr
        cache.purge_images(overlapping_pairs[h+1:])  # delete images we don't need
    return pair_data

def plot_pairs(peak, img_i, img_j, corr, i, j, stage_displacement, conf):
    fig, axs = plt.subplots(2,3)
    axs[0, 0].imshow(img_i)
    axs[0, 0].scatter(peak[1], peak[0], edgecolors = 'red', facecolors = 'none')
    axs[0, 1].imshow(img_j)
    axs[1, 0].imshow(corr)
    axs[1, 0].scatter(peak[1], peak[0], edgecolors = 'red', facecolors = 'none')

    # axs[1, 0].set_xlabel(under_thresh / np.count_nonzero(corr))
    peak = np.asarray(peak).astype(int)
    starting_peak = peak.copy()

    canvas = np.zeros([int(abs(peak[0]) + img_i.shape[0]), int(abs(peak[1]) + img_i.shape[1]), 3])
    if peak[1] >= 0:
        if peak[0] >= 0 :
            canvas[:img_i.shape[0], :img_i.shape[1]] = img_i
            canvas[peak[0]:peak[0]+img_i.shape[0], peak[1]:peak[1]+img_i.shape[1]] = img_j
        else:
            canvas[-peak[0]:-peak[0]+img_i.shape[0], :img_i.shape[1]] = img_i
            canvas[:img_i.shape[0],peak[1]:peak[1]+img_i.shape[1]] = img_j
    else:
        if peak[0] >= 0 :
            canvas[:img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
            canvas[peak[0]:peak[0]+img_i.shape[0], :img_i.shape[1]] = img_j
        else:
            canvas[-peak[0]:-peak[0]+img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
            canvas[:img_i.shape[0], :img_i.shape[1]] = img_j
    axs[1,1].imshow(canvas.astype(np.uint8))
    # plt.show()
    # fig.savefig('pairs\{}_{}.png'.format(i, j))
    # plt.close(fig)

    axs[1, 0].set_xlabel(f'{stage_displacement}, {peak}, \n {[stage_displacement[i] - peak[i] for i in range(len(stage_displacement))]}, {conf}')

    offsets = np.abs([stage_displacement[i] - peak[i] for i in range(len(stage_displacement))])
    axis = np.argmax(offsets)
    peak = starting_peak.copy()
    peak[axis] -= img_i.shape[axis]

    canvas = np.zeros([int(abs(peak[0]) + img_i.shape[0]), int(abs(peak[1]) + img_i.shape[1]), 3])

    if peak[1] >= 0:
        if peak[0] >= 0 :
            canvas[:img_i.shape[0], :img_i.shape[1]] = img_i
            canvas[peak[0]:peak[0]+img_i.shape[0], peak[1]:peak[1]+img_i.shape[1]] = img_j
        else:
            canvas[-peak[0]:-peak[0]+img_i.shape[0], :img_i.shape[1]] = img_i
            canvas[:img_i.shape[0],peak[1]:peak[1]+img_i.shape[1]] = img_j
    else:
        if peak[0] >= 0 :
            canvas[:img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
            canvas[peak[0]:peak[0]+img_i.shape[0], :img_i.shape[1]] = img_j
        else:
            canvas[-peak[0]:-peak[0]+img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
            canvas[:img_i.shape[0], :img_i.shape[1]] = img_j
    axs[1,2].imshow(canvas.astype(np.uint8))

    peak = starting_peak.copy()
    offsets = np.abs([stage_displacement[i] - peak[i] for i in range(len(stage_displacement))])
    axis = np.argmax(offsets)
    peak[axis] += img_i.shape[axis]

    canvas = np.zeros([int(abs(peak[0]) + img_i.shape[0]), int(abs(peak[1]) + img_i.shape[1]), 3])

    if peak[1] >= 0:
        if peak[0] >= 0 :
            canvas[:img_i.shape[0], :img_i.shape[1]] = img_i
            canvas[peak[0]:peak[0]+img_i.shape[0], peak[1]:peak[1]+img_i.shape[1]] = img_j
        else:
            canvas[-peak[0]:-peak[0]+img_i.shape[0], :img_i.shape[1]] = img_i
            canvas[:img_i.shape[0],peak[1]:peak[1]+img_i.shape[1]] = img_j
    else:
        if peak[0] >= 0 :
            canvas[:img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
            canvas[peak[0]:peak[0]+img_i.shape[0], :img_i.shape[1]] = img_j
        else:
            canvas[-peak[0]:-peak[0]+img_i.shape[0],-peak[1]:-peak[1]+img_i.shape[1]] = img_i
            canvas[:img_i.shape[0], :img_i.shape[1]] = img_j
    axs[0,2].imshow(canvas.astype(np.uint8))

    plt.show()
    # plt.savefig(f"{i}_{j}_corr.png")


def break_ties(
        overlay_image: np.ndarray,
        base_image: np.ndarray,
        peak_loc: XYDisplacementInPixels,
    ):
    """Evaluate the correlation of two images at four positions, to break FFT ambiguity"""
    # Ensure peak_loc is a 2-element array, between 0 and the image shape
    # peak_loc may be negative - if that's the case we flip it round
    size = np.array(base_image.shape[:2])
    peak_loc = np.array(peak_loc, dtype=int) % size
    candidates = []
    # Every FFT correlation corresponds to four possible values - except if either
    # element is zero, at which point there's no ambiguity.
    # The comprehension below sets the quadrants we need to investigate.
    # `False` means a positive shift, `True` means a shift of `peak_loc[i]-size[i]`.
    quadrants = [
        [x_negative, y_negative]
        for x_negative in ([True, False] if peak_loc[0] != 0 else [False])
        for y_negative in ([True, False] if peak_loc[1] != 0 else [False])
    ]
    for quadrant in quadrants:
        score = correlation_coefficient(
            base_image[quadrant_slices(peak_loc, quadrant)], 
            overlay_image[quadrant_slices(-peak_loc, quadrant)]
        )
        candidates.append(
            {
                "score": score,
                "loc": peak_loc - size * np.array(quadrant),
                "quadrant": quadrant,
            }
        )

    best_candidate = np.argmax([c["score"] for c in candidates])
    return candidates[best_candidate]["loc"]


def correlation_coefficient(patch1, patch2):
    """Calculate how well two patches of image correlate, i.e. their similarity"""
    if np.product(patch1.shape) == 0 or np.product(patch2.shape) == 0:
        return 0
    product = np.mean((patch1 - patch1.mean()) * (patch2 - patch2.mean()))
    stds = patch1.std() * patch2.std()
    if stds == 0:
        return 0
    else:
        product /= stds
        return product


def quadrant_slices(split_point, quadrant):
    """XY slices to select a quadrant of an image
    
    This function returns a tuple of two slices, which will select
    one quadrant of an image, with its corner at `split_point`.
    
    `split_point` should be a 2 element array of integers between 0 and 
    the image size.
    
    `quadrant` should be a 2 element array of booleans. `False` means
    we take the part of the image starting at `split_point` until the
    end of the axis - `True` will take the image from 0 up to 
    `split_point`.
    
    If either element of `split_point` is negative, we flip the corresponding
    `quadrant` element - so `quadrant_slices([-100, 100], [False, False])`
    will return `[slice(None, -100), slice(100, None)]`.
    """
    slices = []
    for sp, negative in zip(split_point, quadrant):
        if sp < 0:
            # If the split point <0 we will be indexing from the end of
            # the axis - so we flip the quadrant as well.
            negative = not negative
        if negative:
            slices.append(slice(None, sp))
        else:
            slices.append(slice(sp, None))
    return tuple(slices)
