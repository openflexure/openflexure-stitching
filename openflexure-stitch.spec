# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(
    ['pyinstaller\\openflexure-stitch.py'],
    pathex=[],
    binaries=[('vips-dev-8.14\\bin\\libaom.dll', '.'), ('vips-dev-8.14\\bin\\libarchive-13.dll', '.'), ('vips-dev-8.14\\bin\\libbrotlicommon.dll', '.'), ('vips-dev-8.14\\bin\\libbrotlidec.dll', '.'), ('vips-dev-8.14\\bin\\libbrotlienc.dll', '.'), ('vips-dev-8.14\\bin\\libc++.dll', '.'), ('vips-dev-8.14\\bin\\libcairo-2.dll', '.'), ('vips-dev-8.14\\bin\\libcfitsio.dll', '.'), ('vips-dev-8.14\\bin\\libcgif-0.dll', '.'), ('vips-dev-8.14\\bin\\libexif-12.dll', '.'), ('vips-dev-8.14\\bin\\libexpat-1.dll', '.'), ('vips-dev-8.14\\bin\\libffi-8.dll', '.'), ('vips-dev-8.14\\bin\\libfftw3-3.dll', '.'), ('vips-dev-8.14\\bin\\libfontconfig-1.dll', '.'), ('vips-dev-8.14\\bin\\libfreetype-6.dll', '.'), ('vips-dev-8.14\\bin\\libfribidi-0.dll', '.'), ('vips-dev-8.14\\bin\\libgdk_pixbuf-2.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libgio-2.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libglib-2.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libgmodule-2.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libgobject-2.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libharfbuzz-0.dll', '.'), ('vips-dev-8.14\\bin\\libheif.dll', '.'), ('vips-dev-8.14\\bin\\libhwy.dll', '.'), ('vips-dev-8.14\\bin\\libIex-3_1.dll', '.'), ('vips-dev-8.14\\bin\\libIlmThread-3_1.dll', '.'), ('vips-dev-8.14\\bin\\libimagequant.dll', '.'), ('vips-dev-8.14\\bin\\libjpeg-62.dll', '.'), ('vips-dev-8.14\\bin\\libjxl.dll', '.'), ('vips-dev-8.14\\bin\\libjxl_threads.dll', '.'), ('vips-dev-8.14\\bin\\liblcms2-2.dll', '.'), ('vips-dev-8.14\\bin\\libMagickCore-6.Q16-7.dll', '.'), ('vips-dev-8.14\\bin\\libmatio-11.dll', '.'), ('vips-dev-8.14\\bin\\libniftiio.dll', '.'), ('vips-dev-8.14\\bin\\libOpenEXR-3_1.dll', '.'), ('vips-dev-8.14\\bin\\libopenjp2.dll', '.'), ('vips-dev-8.14\\bin\\libopenslide-0.dll', '.'), ('vips-dev-8.14\\bin\\liborc-0.4-0.dll', '.'), ('vips-dev-8.14\\bin\\libpango-1.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libpangocairo-1.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libpangoft2-1.0-0.dll', '.'), ('vips-dev-8.14\\bin\\libpixman-1-0.dll', '.'), ('vips-dev-8.14\\bin\\libpng16-16.dll', '.'), ('vips-dev-8.14\\bin\\libpoppler-130.dll', '.'), ('vips-dev-8.14\\bin\\libpoppler-glib-8.dll', '.'), ('vips-dev-8.14\\bin\\librsvg-2-2.dll', '.'), ('vips-dev-8.14\\bin\\libsharpyuv-0.dll', '.'), ('vips-dev-8.14\\bin\\libspng-0.dll', '.'), ('vips-dev-8.14\\bin\\libsqlite3-0.dll', '.'), ('vips-dev-8.14\\bin\\libtiff-6.dll', '.'), ('vips-dev-8.14\\bin\\libunwind.dll', '.'), ('vips-dev-8.14\\bin\\libvips-42.dll', '.'), ('vips-dev-8.14\\bin\\libvips-cpp-42.dll', '.'), ('vips-dev-8.14\\bin\\libwebp-7.dll', '.'), ('vips-dev-8.14\\bin\\libwebpdemux-2.dll', '.'), ('vips-dev-8.14\\bin\\libwebpmux-3.dll', '.'), ('vips-dev-8.14\\bin\\libxml2-2.dll', '.'), ('vips-dev-8.14\\bin\\libz1.dll', '.'), ('vips-dev-8.14\\bin\\libznz.dll', '.'), ('vips-dev-8.14\\bin\\vips-modules-8.14\\vips-jxl.dll', '.'), ('vips-dev-8.14\\bin\\vips-modules-8.14\\vips-magick.dll', '.'), ('vips-dev-8.14\\bin\\vips-modules-8.14\\vips-openslide.dll', '.'), ('vips-dev-8.14\\bin\\vips-modules-8.14\\vips-poppler.dll', '.')],
    datas=[],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
    name='openflexure-stitch',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
